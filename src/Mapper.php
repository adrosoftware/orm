<?php

namespace Enphpity\Orm;

use Spot\EntityInterface;

class Mapper extends \Spot\Mapper
{
    /**
     * Relation: HasMany
     */
    public function hasMany(EntityInterface $entity, $entityName, $foreignKey, $localValue = null)
    {
        if ($localValue === null) {
            $localValue = $this->primaryKey($entity);
        }

        if (!is_subclass_of($entityName, 'Spot\EntityInterface')) {
            throw new \InvalidArgumentException("Related entity name must be a "
                . "valid entity that extends Spot\Entity. Given '" .  $entityName . "'.");
        }

        return new Relation\HasMany($this, $entityName, $foreignKey, $this->primaryKeyField(), $localValue);
    }

    /**
     * Relation: HasManyThrough
     */
    public function hasManyThrough(EntityInterface $entity, $hasManyEntity, $throughEntity, $selectField, $whereField)
    {
        $localPkField = $this->primaryKeyField();
        $localValue = $entity->$localPkField;

        if (!is_subclass_of($hasManyEntity, 'Spot\EntityInterface')) {
            throw new \InvalidArgumentException("Related entity name must be a "
                . "valid entity that extends Spot\Entity. Given '" .  $hasManyEntity . "'.");
        }

        if (!is_subclass_of($throughEntity, 'Spot\EntityInterface')) {
            throw new \InvalidArgumentException("Related entity name must be a "
                . "valid entity that extends Spot\Entity. Given '" .  $throughEntity . "'.");
        }

        return new Relation\HasManyThrough($this, $hasManyEntity, $throughEntity, $selectField, $whereField, $localValue);
    }

    /**
     * Relation: HasOne
     *
     * HasOne assumes that the foreignKey will be on the foreignEntity.
     */
    public function hasOne(EntityInterface $entity, $foreignEntity, $foreignKey)
    {
        $localKey = $this->primaryKeyField();

        if (!is_subclass_of($foreignEntity, 'Spot\EntityInterface')) {
            throw new \InvalidArgumentException("Related entity name must be a "
                . "valid entity that extends Spot\Entity. Given '" .  $foreignEntity . "'.");
        }

        // Return relation object so query can be lazy-loaded
        return new Relation\HasOne($this, $foreignEntity, $foreignKey, $localKey, $entity->$localKey);
    }

    /**
     * Relation: BelongsTo
     *
     * BelongsTo assumes that the localKey will reference the foreignEntity's
     * primary key. If this is not the case, you probably want to use the
     * 'hasOne' relationship instead.
     */
    public function belongsTo(EntityInterface $entity, $foreignEntity, $localKey)
    {
        if (!is_subclass_of($foreignEntity, 'Spot\EntityInterface')) {
            throw new \InvalidArgumentException("Related entity name must be a "
                . "valid entity that extends Spot\Entity. Given '" .  $foreignEntity . "'.");
        }

        $foreignMapper = $this->getMapper($foreignEntity);
        $foreignKey = $foreignMapper->primaryKeyField();

        // Return relation object so query can be lazy-loaded
        return new Relation\BelongsTo($this, $foreignEntity, $foreignKey, $localKey, $entity->$localKey);
    }
}