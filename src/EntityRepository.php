<?php

namespace Enphpity\Orm;

class EntityRepository
{
    protected $mapper;

    public function __construct($entityClass = false)
    {
        $this->mapper = orm()->mapper($entityClass);
    }

    public function build(array $attributes)
    {
        $data = [];
        foreach ($attributes as $key => $value) {
            $key = $this->getColumeName($key);
            $data[$key] = $value;
        }

        return $this->mapper->build($data);
    }

    public function create(array $attributes)
    {
        $data = [];
        foreach ($attributes as $key => $value) {
            $key = $this->getColumeName($key);
            $data[$key] = $value;
        }

        return $this->mapper->create($data);
    }

    public function insert($attributes)
    {
        $entity = null;
        if (is_array($attributes)) {
            $data = [];
            foreach ($attributes as $key => $value) {
                $key = $this->getColumeName($key);
                $data[$key] = $value;
            }
            $entity = $this->build($data);
        } elseif ($attributes instanceof Entity) {
            $entity = $attributes;
        }

        return $this->mapper->insert($entity);
    }

    public function save(Entity $entity)
    {
        return $this->mapper->save($entity);
    }

    public function store(Entity $entity)
    {
        return $this->save($entity);
    }

    public function get($id)
    {
        return $this->mapper->get($id);
    }

    public function delete(Entity $entity)
    {
        $id = $entity->getId();
        return $this->mapper->delete(['id' => $id]);
    }

    public function find($id)
    {
        return $this->get($id);
    }

    public function __call($method, $args)
    {
        return $this->mapper->$method(...$args);
    }

    protected function getColumeName($input)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }

    protected function getAttibuteName($input, $capitalizeFirstCharacter = false) 
    {
        $str = str_replace('_', '', ucwords($input, '_'));

        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }

        return $str;
    }
}