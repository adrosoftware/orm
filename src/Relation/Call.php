<?php

namespace Enphpity\Orm\Relation;

use Spot\Entity;

trait Call
{
    /**
     * Passthrough for missing methods on expected object result
     */
    public function __call($func, $args)
    {
        if (method_exists('\Spot\Query', $func) || in_array($func, array_keys($this->mapper()->getMapper($this->entityName())->scopes()))) {
            // See if method exists on Query object, and if it does, add query
            // modification to queue to be executed after query is built and
            // ready so that query is not executed immediately
            $this->queryQueue[] = function (Query $query) use ($func, $args) {
                return call_user_func_array([$query, $func], $args);
            };

            return $this;
        } else {
            // See if method exists on destination object after execution
            // (typically either Spot\Entity\Collection or Spot\Entity object)
            $result = $this->execute();
            return call_user_func_array([$result, $func], $args);
        }
    }
}