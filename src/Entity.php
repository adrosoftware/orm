<?php

namespace Enphpity\Orm;

use Enphpity\Orm\Presenter\Presenter;
use Spot\Entity\Collection;
use Spot\EntityInterface;
use Spot\Entity as SpotEntity;

class Entity extends SpotEntity
{
    protected $presenter = Presenter::class;

    public function present()
    {
        $class = $this->presenter;
        return new $class($this);
    }

    protected function getColumeName($input)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }

    protected function getAttibuteName($input, $capitalizeFirstCharacter = false) 
    {
        $str = str_replace('_', '', ucwords($input, '_'));

        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }

        return $str;
    }


    protected function getMutatorMethod($type, $propertyName)
    {
        return $type . ucfirst($propertyName);
    } 

    public function __call($method, $arguments)
    {
        $property = $this->getPropertyNameFromMethod($method);

        if ($this->mutatorType($method) == 'get') {
            $property = $this->getColumeName($property);
            return $this->get($property);
        }

        if ($this->mutatorType($method) == 'set') {
            $property = $this->getColumeName($property);
            $this->set($property, $arguments[0]);
            return $this;
        }
        throw new \BadMethodCallException("Method " . get_called_class() . "::$method does not exist");
    }

    protected function mutatorType($method)
    {
        if (strncasecmp($method, 'get', 3) === 0) {
            return 'get';
        }

        if (strncasecmp($method, 'set', 3) === 0) {
            return 'set';
        }
    }

    protected function getPropertyNameFromMethod($method)
    {
        return lcfirst(substr($method, 3));
    }


    /**
     * Gets and sets data on the current entity
     *
     * @param null|array $data
     * @param bool $modified
     * @param boolean $loadRelations Determine if you want to load entity relations
     * @return $this|array|null
     */
    public function data($data = null, $modified = true, $loadRelations = true)
    {
        $entityName = get_class($this);

        // GET
        if (null === $data || !$data) {
            $data = array_merge($this->_data, $this->_dataModified);
            foreach ($data as $k => &$v) {
                $k = $this->getColumeName($k);
                $v = $this->__get($k);
            }

            if ($loadRelations) {
                foreach (self::$relationFields[$entityName] as $relationField) {
                    $relation = $this->relation($relationField);

                    if ($relation instanceof Collection) {
                        $data[$relationField] = $relation->toArray();
                    }

                    if ($relation instanceof EntityInterface) {
                        $data[$relationField] = $relation->data(null, $modified, false);
                    }
                }
            }

            return $data;
        }

        // SET
        if (is_object($data) || is_array($data)) {
            foreach ($data as $k => $v) {
                $k = $this->getColumeName($k);
                $this->set($k, $v, $modified);
            }

            return $this;
        } else {
            throw new \InvalidArgumentException(__METHOD__ . " Expected array or object input - " . gettype($data) . " given");
        }
    }
}