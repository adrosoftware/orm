<?php

namespace Enphpity\Orm;

class Enphpity
{
    protected $orm;

    public function __construct(array $config)
    {
        $cfg = new \Spot\Config();

        $default = $config['connections'][$config['default']];

        $cfg->addConnection($config['default'], $default);

        unset($config['connections'][$config['default']]);

        foreach ($config['connections'] as $name => $connectionParams) {
            $cfg->addConnection($name, $connectionParams);
        }

        $this->orm = new Locator($cfg);
    }

    public function orm()
    {
        return $this->orm;
    }
}