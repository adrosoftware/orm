<?php

namespace Enphpity\Orm;

class Locator extends \Spot\Locator
{
    /**
     * Get mapper for specified entity
     *
     * @param  string      $entityName Name of Entity object to load mapper for
     * @return \Axiuayan\Orm
     */
    public function mapper($entityName)
    {
        if (!isset($this->mapper[$entityName])) {
            // Get custom mapper, if set
            $mapper = $entityName::mapper();
            // Fallback to generic mapper
            if ($mapper === false) {
                $mapper = Mapper::class;
            }
            $this->mapper[$entityName] = new $mapper($this, $entityName);
        }

        return $this->mapper[$entityName];
    }
}