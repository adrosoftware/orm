<?php

namespace Enphpity\Orm\Presenter;

use Spot\EntityInterface;

class Presenter
{
    protected $entity;

    public function __construct(EntityInterface $entity)
    {
        $this->entity = $entity;
    }

    public function __get($name)
    {
        if (property_exists($this->entity, $name) && method_exists($this, $name)){
            return $this->$name();
        }
    }

    public function __call($name, $arguments)
    {
        if (property_exists($this->entity, $name)) {
            $method = "get" . ucfirst($name);
            return $this->entity->$method();
        }
    }

    public function __isset($name)
    {
        return property_exists($this->entity, $name);
    }
}
